<%--
  Created by IntelliJ IDEA.
  User: Leo G
  Date: 21/07/2020
  Time: 15:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add products to list</title>
</head>
<body>
    <form action="post">
        <label for="name">Name
            <input type="text" name="name" id="name">
        </label>
        <label for="description">Description
            <textarea type="text" name="description" id="description">
        </label>
        <label for="price">Prix
            <input type="text" name="price" id="price">
        </label>
        <button type="submit" value="add"></button>
    </form>
</body>
</html>
