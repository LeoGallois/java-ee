<%--
  Created by IntelliJ IDEA.
  User: lohmf
  Date: 05/10/2019
  Time: 22:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add new user</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div>
    <h1>Add</h1>
</div>

<div>
    <c:choose>
        <c:when test="${ !empty username}"><p> User '${username}' added !</p></c:when>
        <c:when test="${username =='Already existing'}"><p> ${username} </p></c:when>
        <c:otherwise></c:otherwise>
    </c:choose>

    <div>
        <form method="post">
            <label>Name:
                <input type="text" name="username" id="username"><br />
            </label>
            <label>Password:
                <input type="password" name="password" id="password"><br />
            </label>
            <button type="submit">Submit</button>
        </form>
    </div>
</div>

<div>
    <button onclick="location.href='/'">Back to main</button>
</div>
</body>
</html>
