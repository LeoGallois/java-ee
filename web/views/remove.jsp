<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div>
    <h1>Remove</h1>
</div>
<div>

    <c:choose>
        <c:when test="${ !empty username}"><p> User '${username}' remove !</p></c:when>
        <c:when test="${username == 'Not found !'}"><p> User '${username}' remove !</p></c:when>
        <c:otherwise></c:otherwise>
    </c:choose>
    <form method="post">
        <label>Name:
            <input type="text" name="username" id="username"><br/>
        </label>
        <button type="submit">Submit</button>
    </form>
</div>
<div>
    <button onclick="location.href='/'">Back to main</button>
</div>
</body>
</html>
