<%--
  Created by IntelliJ IDEA.
  User: lohmf
  Date: 05/10/2019
  Time: 22:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Connect to user</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>
<div>
    <h1>Connection</h1>
</div>

<div>
    <div>
        <c:choose>
            <c:when test="${sessionScope.logged == false}"><p>Your credentials are wrong, please check it</p></c:when>
            <c:when test="${sessionScope.logged == true}"><p>Your credentials are good, you will be redirect in few instants</p></c:when>
        </c:choose>

        <form method="post">
            <label>Username:
                <input type="text" name="username" id="username"><br />
            </label>
            <label>Password:
                <input type="password" name="password" id="password"><br />
            </label>
            <button type="submit">Submit</button>
        </form>
    </div>
</div>

<div>
    <button onclick="location.href='/'">Back to main</button>
</div>
</body>
</html>
