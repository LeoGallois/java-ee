<%@ page import="java.util.List" %>
<html>
<head>
    <title>Users</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>

<body>

<div>
    <div>
        <div>
            <h2>Users</h2>
        </div>
        <c:if test="${ !empty usernames }">
            <c:forEach items="${ usernames }" var="user" varStatus="status">
                <li>${ user.name } ${user.password}</li>
            </c:forEach>
        </c:if>
    </div>
</div>

<div>
    <button onclick="location.href='/'">Back to main</button>
</div>
</body>
</html>