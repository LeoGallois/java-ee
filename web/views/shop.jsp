<html>
<head>
    <title>Shop</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

</head>
<body>
<div>
    <div>
        <h1>Shop</h1>
    </div>

    <div>
        <ul>
        <c:if test="${ !empty products }">
            <c:forEach items="${ products }" var="product" varStatus="status">
                <li>
                    ${ product.getName() } ${product.getDescription()} ${product.getPrice()}
                    <button value="Add to your cart" onclick="${product.addProduct()}"></button>
                </li>
            </c:forEach>
        </c:if>
        </ul>
    </div>
</div>
</body>
</html>
