<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Project</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
</head>
<body>

<div>
    <h1>Menu</h1>
</div>

<div>
    <div>
        <button onclick="location.href='/list'">List users</button>
        <button onclick="location.href='/add'">Add user</button>
        <button onclick="location.href='/remove'">Remove user</button>
        <button onclick="location.href='/connection'">Connection user</button>
    </div>
    <div>
        <c:if test="${sessionScope.logged == true}">
            <button onclick="location.href='/shop'">Shop</button>
            <button onclick="location.href='/cart'">Your Cart</button>
        </c:if>
    </div>
</div>
</body>
</html>