package fr.azerks.controller.servlets;

import fr.azerks.Database.MongoUserController;
import fr.azerks.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ConnectionServlet", urlPatterns = "/connection")
public class ConnectionServlet extends HttpServlet {

    private MongoUserController mongo = MongoUserController.getInstance();

    // TODO: 13/10/2019 CHANGE LOGIN SYSTEM BY STATEMENT "OK, WRONG, ALREADY, ERROR"
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User user = mongo.getUserFromDB(username);

        if(user != null){
            if (user.getName().equalsIgnoreCase(username) && user.getPassword().equalsIgnoreCase(password)){
                // Authorize connection
                user.setLogged(true);

                HttpSession session = request.getSession();
                session.setAttribute("logged", true);
                session.setAttribute("user", user);
            }
        }else {
            request.setAttribute("logged", false);
        }

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/views/connection.jsp").forward(request, response);
    }
}
