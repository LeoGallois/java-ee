package fr.azerks.controller.servlets;

import fr.azerks.Database.MongoShopController;
import fr.azerks.Database.MongoUserController;
import fr.azerks.model.Product;
import fr.azerks.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "ShopServlet", urlPatterns = "/shop")
public class ShopServlet extends HttpServlet {

    private MongoShopController mongoShop = MongoShopController.getInstance();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");

        request.setAttribute("products", mongoShop.getAllProducts());

        this.getServletContext().getRequestDispatcher("/views/shop.jsp").forward(request, response);
    }
}
