package fr.azerks.controller.servlets;

import fr.azerks.Database.MongoUserController;
import fr.azerks.model.Product;
import fr.azerks.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

@WebServlet(name = "add", urlPatterns = "/add")
public class AddServlet extends HttpServlet {

    private User user = null;
    private MongoUserController mongo = MongoUserController.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String pass = request.getParameter("password");

        user = mongo.getUserFromDB(username);
        if (user == null){
            HashSet<Product> p = new HashSet<>();
            p.add(new Product("Tv", "Tv 50", 10000, 1));

            mongo.createUser(username, pass, p);
            request.setAttribute("username", username);
        }else {
            String message = "Already existing";
            request.setAttribute("username", message);
        }


        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/views/add.jsp").forward(request, response);
    }
}
