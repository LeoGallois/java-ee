package fr.azerks.controller.servlets;

import fr.azerks.Database.MongoUserController;
import fr.azerks.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "remove", urlPatterns = "/remove")
public class RemoveServlet extends HttpServlet {

    // TODO: 13/10/2019 REMVOVE ALL
    private User user = null;
    private MongoUserController mongo = MongoUserController.getInstance();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        user = mongo.getUserFromDB(request.getParameter("username"));
        if (user != null){
            mongo.removeUser(user.getName());
            request.setAttribute("username", user.getName());
        }else {
            request.setAttribute("username", "Not found !");
        }


        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.getServletContext().getRequestDispatcher("/views/remove.jsp").forward(request, response);
    }
}
