package fr.azerks.controller.servlets;

import fr.azerks.Database.MongoUserController;
import fr.azerks.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "list", urlPatterns = "/list")
public class ListServlet extends HttpServlet {

    private MongoUserController mongo = MongoUserController.getInstance();


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<User> names = mongo.getAllUser();
        request.setAttribute("usernames", names);

        this.getServletContext().getRequestDispatcher("/views/list.jsp").forward(request, response);
    }
}
