package fr.azerks.Database;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import fr.azerks.model.Product;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class MongoShopController extends MongoController{

    private static MongoShopController instance = null;
    private MongoDatabase db;
    private MongoCollection<Document> productsCollection;

    public static MongoShopController getInstance() {
        if (instance == null){
            instance = new MongoShopController();
            instance.connection();
            instance.db = instance.getDb();
            instance.getCollection("products");
            return instance;
        }
        return instance;
    }

    public MongoCollection getCollection(String name) {
        if (db != null){
            return productsCollection = db.getCollection(name);
        }
        return null;
    }

    public void addProduct(Product product){
        if(!productExist(product)){
            Document doc = new Document("name", product.getName())
                    .append("desc", product.getDescription())
                    .append("price", product.getPrice());
            productsCollection.insertOne(doc);
        }
    }

    public void addProducts(Product... products){
        List<Document> docs = new ArrayList<>();
        for (Product product: products) {
            if(!productExist(product)){
                Document doc = new Document("name", product.getName())
                        .append("desc", product.getDescription())
                        .append("price", product.getPrice());
                docs.add(doc);
            }
        }
        productsCollection.insertMany(docs);

    }

    public void removeProduct(Product product){
        if (productExist(product)){
            productsCollection.deleteOne(eq("name", product.getName()));
        }
    }

    public List<Product> getAllProducts(){
        List<Product> products = new ArrayList<>();
        FindIterable<Document> documents = productsCollection.find();
        for (Document doc: documents) {
            String name = doc.getString("name");
            String desc = doc.getString("desc");
            double price = doc.getDouble("price");

            products.add(new Product(name, desc, price));
        }
        return products;
    }

    private boolean productExist(Product product) {
        Document query = productsCollection.find(eq("name", product.getName())).first();
        if (query != null){
            if (query.get("name").toString().equalsIgnoreCase(product.getName())){
                return true;
            }
        }
        return false;
    }

}
