package fr.azerks.Database;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import fr.azerks.model.Product;
import fr.azerks.model.User;
import org.bson.BasicBSONObject;
import org.bson.Document;

import java.util.*;

import static com.mongodb.client.model.Filters.eq;

public class MongoUserController extends MongoController{

    private static MongoUserController instance = null;
    private MongoDatabase db;
    private MongoCollection<Document> userCollection;

    public static MongoUserController getInstance() {
        if (instance == null){
            instance = new MongoUserController();
            instance.connection();
            instance.db = instance.getDb();
            instance.getCollection("user");
            return instance;
        }
        return instance;
    }

    public MongoCollection getCollection(String name) {
        if (db != null){
            return userCollection = db.getCollection(name);
        }
        return null;
    }

    public void createUser(String username, String password, HashSet<Product> cart) {
        if (!hasAccount(username)) {
            Document doc = new Document("username", username)
                    .append("password", password)
                    .append("cart", cart);
            userCollection.insertOne(doc);
        }
    }

    public boolean hasAccount(String username) {
        Document query = userCollection.find(eq("username", username)).first();
        if (query != null) {
            if (query.get("username").toString().equalsIgnoreCase(username)) {
                return true;
            }
        }
        return false;
    }

    public User getUserFromDB(String name) {
        if (hasAccount(name)) {
            String username;
            String password;
            HashSet<Product> cart;

            Document document = userCollection.find(eq("username", name)).first();
            if (document != null || document.size() != 0) {

                username = document.getString("username");
                password = document.getString("password");

                cart = (HashSet<Product>) document.get("cart");
                if (cart != null) return new User(username, password, cart);

                return new User(username, password, new HashSet<>());
            }
        }
        return null;
    }

    public List<User> getAllUser(){
        List<User> users = new ArrayList<>();
        FindIterable<Document> document = userCollection.find();
        for (Document doc: document) {
            String username = doc.get("username").toString();
            String password = doc.get("password").toString();
            HashSet<Product> cart = null;
            try {
                cart = (HashSet<Product>) doc.get("cart");
            }catch (ClassCastException e){
                e.getStackTrace();
            }

            if (cart != null){
                users.add(new User(username, password, cart));
            }else {
                users.add(new User(username, password, new HashSet<>()));
            }
        }
        return users;
    }

    public void removeUser(String username){
        if (hasAccount(username)){
            userCollection.deleteOne(eq("username", username));
        }
    }
}
