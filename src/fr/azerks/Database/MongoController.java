package fr.azerks.Database;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public abstract class MongoController {

    MongoClient mongoClient = null;
    MongoDatabase db = null;
    private String host = "localhost";
    private int port = 27017;

    private ConnectionString connection = new ConnectionString("mongodb+srv://dbUser:&A3%3A>dvt3!yqx&@cluster0.os4gl.mongodb.net/<web>?retryWrites=true&w=majority");

    public void connection() {
        if (!isConnected()) {
            MongoClientSettings settings = MongoClientSettings.builder().applyConnectionString(connection).retryWrites(true).build();
            mongoClient = MongoClients.create(settings);
            db = mongoClient.getDatabase("web");
        }
    }

    private boolean isConnected() {
        return mongoClient != null;
    }

    public void disconnect() {
        mongoClient.close();
    }

    public MongoDatabase getDb() {
        return db;
    }
}
