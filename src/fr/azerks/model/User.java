package fr.azerks.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class User {

    private String name;
    private String password;
    private Boolean isLogged = false;
    public HashSet<Product> cart;

    public User(String name, String password, HashSet<Product> cart) {
        this.name = name;
        this.password = password;
        this.cart = cart;
    }

    public void addProduct(Product product){
         cart.add(product);
    }

    public HashSet<Product> getProducts(String username, String password, HashSet<Product> cart) {
        return cart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean isLogged() {
        return isLogged;
    }

    public void setLogged(Boolean log) {
        isLogged = log;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", cart='" + cart + '\'' +
                '}';
    }
    public double getTotal() {
        double total = 0;
        for (Product entry : cart) {
            total += entry.getPrice() * entry.getQuantity();
        }
        return total;
    }

}
