package fr.azerks.model;

import fr.azerks.Database.MongoShopController;

public class Product {

    private String name;
    private String description;
    private double price;
    private int quantity;

    public Product(String name, String description, double price,int quantity) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
    }

    public void look(){
        System.out.println(String.format(name + " : " + price + "%n" + description));
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void addProduct(){
        MongoShopController.getInstance().addProduct(this);
    }
}
